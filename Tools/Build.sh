#!/bin/bash

# Add Local EmScriptEn For Shell
cd ../Build/emsdk
# Activate PATH and other environment variables in the current terminal
source ./emsdk_env.sh
cd ..


# Compile
cp ../Source/* . -r
emcc main.cpp -std=c++11 -s WASM=1 -s USE_SDL=2 -O3 -o index.js